<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151224130544 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_item CHANGE delivery_cost delivery_cost NUMERIC(10, 2) NOT NULL, CHANGE total_value total_value NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_product CHANGE price price NUMERIC(10, 2) NOT NULL, CHANGE total total NUMERIC(10, 2) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_item CHANGE delivery_cost delivery_cost NUMERIC(4, 2) NOT NULL, CHANGE total_value total_value NUMERIC(4, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_product CHANGE price price NUMERIC(4, 2) NOT NULL, CHANGE total total NUMERIC(4, 2) NOT NULL');
    }
}
