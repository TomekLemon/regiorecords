<?php

namespace Ls\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewsType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('published_at', 'date', array(
            'label' => 'Data publikacji',
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy'
        ));
        $builder->add('content', null, array(
            'label' => 'Treść'
        ));
        $builder->add('video_url', null, array(
            'label' => 'Adres URL linku "Zobacz teledysk"'
        ));
        $builder->add('record_url', null, array(
            'label' => 'Adres URL linku "Zamów płytę na empik.com"'
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();

            if (!$object || null === $object->getId()) {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new File(array(
                            'maxSize' => 2097152,
                            'maxSizeMessage' => 'Maksymalny rozmiar pliku to 2 MB',
                        )),
                        new Image(array(
                            'minWidth' => 640,
                            'minHeight' => 364,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 640px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 364px',
                        ))
                    )
                ));
            } else {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new File(array(
                            'maxSize' => 2097152,
                            'maxSizeMessage' => 'Maksymalny rozmiar pliku to 2 MB',
                        )),
                        new Image(array(
                            'minWidth' => 640,
                            'minHeight' => 364,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 640px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 364px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\NewsBundle\Entity\News',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_news';
    }
}
