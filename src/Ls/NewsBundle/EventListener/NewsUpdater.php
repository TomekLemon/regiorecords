<?php

namespace Ls\NewsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\NewsBundle\Entity\News;

class NewsUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            $entity->deletePhoto();
        }
    }

}