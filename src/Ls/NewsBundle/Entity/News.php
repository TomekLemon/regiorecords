<?php

namespace Ls\NewsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * News
 * @ORM\Table(name="news")
 * @ORM\Entity
 */
class News {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $video_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $record_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $published_at;

    protected $file;

    protected $bigDesktopWidth    = 640;
    protected $bigDesktopHeight   = 364;
    protected $smallDesktopWidth  = 320;
    protected $smallDesktopHeight = 364;
    protected $bigMobileWidth     = 600;
    protected $bigMobileHeight    = 364;
    protected $smallMobileWidth   = 300;
    protected $smallMobileHeight  = 364;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->published_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return News
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set video_url
     *
     * @param string $video_url
     * @return News
     */
    public function setVideoUrl($video_url) {
        $this->video_url = $video_url;

        return $this;
    }

    /**
     * Get video_url
     *
     * @return string
     */
    public function getVideoUrl() {
        return $this->video_url;
    }

    /**
     * Set record_url
     *
     * @param string $record_url
     * @return News
     */
    public function setRecordUrl($record_url) {
        $this->record_url = $record_url;

        return $this;
    }

    /**
     * Get record_url
     *
     * @return string
     */
    public function getRecordUrl() {
        return $this->record_url;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return News
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return News
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return News
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set published_at
     *
     * @param \DateTime $published_at
     * @return News
     */
    public function setPublishedAt($published_at) {
        $this->published_at = $published_at;

        return $this;
    }

    /**
     * Get published_at
     *
     * @return \DateTime
     */
    public function getPublishedAt() {
        return $this->published_at;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'big_desktop':
                $size['width'] = $this->bigDesktopWidth;
                $size['height'] = $this->bigDesktopHeight;
                break;
            case 'small_desktop':
                $size['width'] = $this->smallDesktopWidth;
                $size['height'] = $this->smallDesktopHeight;
                break;
            case 'big_mobile':
                $size['width'] = $this->bigMobileWidth;
                $size['height'] = $this->bigMobileHeight;
                break;
            case 'small_mobile':
                $size['width'] = $this->smallMobileWidth;
                $size['height'] = $this->smallMobileHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_bd');
                    break;
                case 'small_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_sd');
                    break;
                case 'big_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_bm');
                    break;
                case 'small_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_sm');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_bd');
                    break;
                case 'small_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_sd');
                    break;
                case 'big_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_bm');
                    break;
                case 'small_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_sm');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_bd = Tools::thumbName($filename, '_bd');
            $filename_sd = Tools::thumbName($filename, '_sd');
            $filename_bm = Tools::thumbName($filename, '_bm');
            $filename_sm = Tools::thumbName($filename, '_sm');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_bd)) {
                @unlink($filename_bd);
            }
            if (file_exists($filename_sd)) {
                @unlink($filename_sd);
            }
            if (file_exists($filename_bm)) {
                @unlink($filename_bm);
            }
            if (file_exists($filename_sm)) {
                @unlink($filename_sm);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/news';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs() {
        $sFileName = $this->getPhoto();
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameBD = Tools::thumbName($sSourceName, '_bd');
        $aThumbSizeBD = $this->getThumbSize('big_desktop');
        $thumb->adaptiveResize($aThumbSizeBD['width'] + 2, $aThumbSizeBD['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeBD['width'], $aThumbSizeBD['height']);
        $thumb->save($sThumbNameBD);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameSD = Tools::thumbName($sSourceName, '_sd');
        $aThumbSizeSD = $this->getThumbSize('small_desktop');
        $thumb->adaptiveResize($aThumbSizeSD['width'] + 2, $aThumbSizeSD['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeSD['width'], $aThumbSizeSD['height']);
        $thumb->save($sThumbNameSD);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameBM = Tools::thumbName($sSourceName, '_bm');
        $aThumbSizeBM = $this->getThumbSize('big_mobile');
        $thumb->adaptiveResize($aThumbSizeBM['width'] + 2, $aThumbSizeBM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeBM['width'], $aThumbSizeBM['height']);
        $thumb->save($sThumbNameBM);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameSM = Tools::thumbName($sSourceName, '_sm');
        $aThumbSizeSM = $this->getThumbSize('small_mobile');
        $thumb->adaptiveResize($aThumbSizeSM['width'] + 2, $aThumbSizeSM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeSM['width'], $aThumbSizeSM['height']);
        $thumb->save($sThumbNameSM);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}