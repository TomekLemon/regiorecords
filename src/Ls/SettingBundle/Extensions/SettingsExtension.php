<?php

namespace Ls\SettingBundle\Extensions;

use Ls\SettingBundle\Utils\Config;

class SettingsExtension extends \Twig_Extension {

    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'cms_config' => $this->cms_config,
            'settings' => $this->cms_config->all(),
            'news_left' => $this->cms_config->newsLeft(),
            'demo_form' => $this->cms_config->demoForm(),
            'change_password_form' => $this->cms_config->changePasswordForm(),
            'temporary_file_folder' => $this->cms_config->temporaryFileFolder(),
        );
    }

    public function getName() {
        return 'cms_config';
    }

}