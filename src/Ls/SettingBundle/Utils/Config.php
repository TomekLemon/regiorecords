<?php

namespace Ls\SettingBundle\Utils;

use Doctrine\ORM\EntityManager;
use Ls\CoreBundle\Entity\TemporaryFile;
use Ls\DemoBundle\Form\DemoType;
use Ls\SettingBundle\Entity\Setting;
use Ls\UserBundle\Form\UserChangePasswordType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Config {

    protected $em;
    protected $repo;
    protected $container;
    protected $settings  = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->settings)) {
            return $this->settings[$name];
        }
        return $default;
    }

    public function all() {
        $settings = array();

        if ($this->is_loaded) {
            return $this->settings;
        }

        foreach ($this->getRepo()->findAll() as $setting) {
            $settings[$setting->getLabel()] = $setting->getValue();
        }

        $this->settings = $settings;
        $this->is_loaded;

        return $settings;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new Setting()));
        }

        return $this->repo;
    }

    public function newsLeft() {
        $news = $this->em->createQueryBuilder()
            ->select('n')
            ->from('LsNewsBundle:News', 'n')
            ->getQuery()
            ->getResult();

        return $news;
    }

    public function changePasswordForm() {
        $form = $this->container->get('form.factory')->create(new UserChangePasswordType(), null, array(
            'action' => $this->container->get('router')->generate('ls_user_change_password'),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Wyślij'));

        return $form->createView();
    }

    public function demoForm() {
        $form = $this->container->get('form.factory')->create(new DemoType(), null, array(
            'action' => $this->container->get('router')->generate('ls_demo_send'),
            'method' => 'PUT',
        ));

        return $form->createView();
    }

    public function temporaryFileFolder() {
        $entity = new TemporaryFile();
        return $entity->getUploadRootDir();
    }

}
