<?php

namespace Ls\BookingBundle\Controller;

use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Booking controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Booking entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('m')
            ->from('LsBookingBundle:Booking', 'm')
            ->orderBy('m.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($entities as $entity) {
            $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 160, '...'));
        }

        return $this->render('LsBookingBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Booking entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsBookingBundle:Booking', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Booking entity.');
        }

        return $this->render('LsBookingBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
}
