<?php

namespace Ls\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Product controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Product entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('e')
            ->from('LsProductBundle:Product', 'e')
            ->orderBy('e.premier', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsProductBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Product entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsProductBundle:Product', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        return $this->render('LsProductBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
}
