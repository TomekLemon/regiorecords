<?php

namespace Ls\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('author', null, array(
            'label' => 'Autor',
        ));
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('premier', 'date', array(
            'label' => 'Data premiery',
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy'
        ));
        $builder->add('price', 'number', array(
            'label' => 'Cena',
            'scale' => 2,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('quantity', 'integer', array(
            'label' => 'Ilość',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        $builder->add('content', null, array(
            'label' => 'Treść'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj opcje SEO'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'SEO Title'
        ));
        $builder->add('seo_keywords', 'textarea', array(
            'label' => 'SEO Keywords'
        ));
        $builder->add('seo_description', 'textarea', array(
            'label' => 'SEO Description',
            'attr' => array(
                'rows' => 3
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();

            if (!$object || null === $object->getId()) {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new File(array(
                            'maxSize' => 2097152,
                            'maxSizeMessage' => 'Maksymalny rozmiar pliku to 2 MB',
                        )),
                        new Image(array(
                            'minWidth' => 397,
                            'minHeight' => 397,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 397px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 397px',
                        ))
                    )
                ));
            } else {
                $form->add('file', 'file', array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new File(array(
                            'maxSize' => 2097152,
                            'maxSizeMessage' => 'Maksymalny rozmiar pliku to 2 MB',
                        )),
                        new Image(array(
                            'minWidth' => 397,
                            'minHeight' => 397,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 397px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 397px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\ProductBundle\Entity\Product',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_product';
    }
}
