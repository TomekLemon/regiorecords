<?php

namespace Ls\UserBundle\Controller;

use Ls\CoreBundle\Utils\Tools;
use Ls\UserBundle\Entity\User;
use Ls\UserBundle\Form\UserChangePasswordType;
use Ls\UserBundle\Form\UserProfileType;
use Ls\UserBundle\Form\UserRegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends Controller {
    public function loginAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('LsUserBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function loginCheckAction() {
        return new Response('OK');
    }

    public function logoutAction() {
        return new Response('OK');
    }

    public function profileAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('ls_core_homepage'));
        }

        $entity = $this->get('security.token_storage')->getToken()->getUser();

        $form = $this->createForm(new UserProfileType(), $entity, array(
            'action' => $this->generateUrl('ls_user_profile'),
            'method' => 'POST'
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        }

        $errors = Tools::getFormErrors($form);

        return $this->render('LsUserBundle:Security:profile.html.twig', array(
            'form' => $form->createView(),
            'errors' => $errors,
        ));
    }

    public function registerAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new User();
        $success = false;

        $form = $this->createForm(new UserRegisterType(), $entity, array(
            'action' => $this->container->get('router')->generate('ls_user_register'),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Zapisz'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($entity, $entity->getPassword());
            $entity->setPassword($encoded);
            $entity->setRoles(array("ROLE_USER"));
            $entity->setActive(false);
            $entity->setActivationGuid(Tools::generateGuid());

            $em->persist($entity);
            $em->flush();

            $url = $this->get('router')->generate('ls_user_activate', array('guid' => $entity->getActivationGuid()), true);

            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Witaj ' . $entity->getFirstname() . ',</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Aby aktywować konto założone w sklepie RegioRecords i móc w pełni z niego korzystać kliknij w link znajdujący się poniżej:</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><a href="' . $url . '">Aktywuj konto</a></p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">pozdrawiamy i życzymy udanych zakupów,</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zespół Regio Records</p>';

            $email_to = $entity->getEmail();

            $message = \Swift_Message::newInstance();
            $message->setSubject('Aktywacja konta w portalu regiorecords.pl');
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email_to);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $success = true;
        }

        $errors = Tools::getFormErrors($form);

        return $this->render('LsUserBundle:Security:register.html.twig', array(
            'form' => $form->createView(),
            'errors' => $errors,
            'success' => $success,
        ));
    }

    public function activateAction($guid) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsUserBundle:User', 'p')
            ->where('p.activation_guid = :guid')
            ->setParameter('guid', $guid)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity) {
            $entity->setActive(true);
            $entity->setActivationGuid(null);

            $em->persist($entity);
            $em->flush();

            $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());

            $this->get("security.token_storage")->setToken($token);

            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        }

        return $this->render('LsUserBundle:Security:activate.html.twig', array(
            'entity' => $entity
        ));
    }

    public function changePasswordAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $response = array(
            'result' => 'ERROR',
            'form' => '',
            'errors' => array()
        );

        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $response['result'] = 'IS_NOT_AUTHENTICATED';

            return new JsonResponse($response);
        }

        $form = $this->createForm(new UserChangePasswordType(), null, array(
            'action' => $this->container->get('router')->generate('ls_user_change_password'),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Wyślij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity = $this->get('security.token_storage')->getToken()->getUser();

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($entity, $form->get('newPassword')->getData());
            $entity->setPassword($encoded);

            $em->persist($entity);
            $em->flush();

            $form = $this->createForm(new UserChangePasswordType(), null, array(
                'action' => $this->container->get('router')->generate('ls_user_change_password'),
                'method' => 'PUT',
            ));
            $form->add('submit', 'submit', array('label' => 'Wyślij'));

            $response['form'] = iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsUserBundle:Security:change_password.html.twig', array(
                'form' => $form->createView()
            ))->getContent());

            $response['result'] = 'OK';
        } else {
            $response['errors'] = Tools::getFormErrors($form);
        }

        return new JsonResponse($response);
    }

    public function loginAdminAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('LsUserBundle:Security:loginAdmin.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function loginAdminCheckAction() {
        return new Response('OK');
    }

    public function logoutAdminAction() {
        return new Response('OK');
    }
}
