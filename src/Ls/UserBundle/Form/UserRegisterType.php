<?php

namespace Ls\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserRegisterType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', null, array(
            'label' => 'Login:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'first_options' => array('label' => 'Hasło:'),
            'second_options' => array('label' => 'Powtórz hasło:'),
            'invalid_message' => 'Hasła nie są takie same',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('firstname', null, array(
            'label' => 'Imię:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('lastname', null, array(
            'label' => 'Nazwisko:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('phone', null, array(
            'label' => 'Telefon:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('email', null, array(
            'label' => 'E-mail:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail'
                ))
            )
        ));
        $builder->add('street', null, array(
            'label' => 'Ulica i nr domu:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('zip', null, array(
            'label' => 'Kod pocztowy:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('city', null, array(
            'label' => 'Miasto:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_user_register';
    }
}
