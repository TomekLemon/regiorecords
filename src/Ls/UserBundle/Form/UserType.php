<?php

namespace Ls\UserBundle\Form;

use Ls\UserBundle\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', null, array(
            'label' => 'Nazwa konta',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail'
                ))
            )
        ));
        $builder->add('firstname', null, array(
            'label' => 'Imię'
        ));
        $builder->add('lastname', null, array(
            'label' => 'Nazwisko'
        ));
        $builder->add('phone', null, array(
            'label' => 'Telefon'
        ));
        $builder->add('street', null, array(
            'label' => 'Ulica i nr domu'
        ));
        $builder->add('zip', null, array(
            'label' => 'Kod pocztowy'
        ));
        $builder->add('city', null, array(
            'label' => 'Miasto'
        ));
        $builder->add('active', null, array(
            'label' => 'Aktywne'
        ));
        $builder->add('roles', 'choice', array(
            'label' => 'Role',
            'choices' => User::getRolesOptions(),
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'empty_value' => false,
            'constraints' => array(
                new Count(array(
                    'min' => 1,
                    'minMessage' => 'Wybierz przynajmniej jedną rolę'
                ))
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $user = $event->getData();
            $form = $event->getForm();

            if (!$user || null === $user->getId()) {
                $form->add('plain_password', 'text', array(
                    'label' => 'Hasło',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole'
                        ))
                    )
                ));
            } else {
                $form->add('plain_password', 'text', array(
                    'label' => 'Hasło'
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_user';
    }
}
