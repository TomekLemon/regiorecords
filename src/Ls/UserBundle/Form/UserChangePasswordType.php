<?php

namespace Ls\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserChangePasswordType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('newPassword', 'repeated', array(
            'type' => 'password',
            'first_options' => array('label' => 'Nowe hasło'),
            'second_options' => array('label' => 'Powtórz nowe hasło'),
            'invalid_message' => 'Hasła nie są takie same',
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_user_password';
    }
}
