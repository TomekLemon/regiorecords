<?php

namespace Ls\BasketBundle\Controller;

use Ls\OrderBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Basket controller.
 *
 */
class FrontController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        return $this->render('LsBasketBundle:Front:index.html.twig', array());
    }

    public function customerDataAction() {
        $em = $this->getDoctrine()->getManager();

        return $this->render('LsBasketBundle:Front:customerData.html.twig', array());
    }

    public function summaryAction() {
        $em = $this->getDoctrine()->getManager();

        return $this->render('LsBasketBundle:Front:summary.html.twig', array());
    }

    public function payuAction($payu_session_id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.payu_session_id = :payu_session_id')
            ->setParameter('payu_session_id', $payu_session_id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity) {
            if (null === $entity->getPayuStartedAt()) {
                $amount = round($entity->getTotalValue(), 2, PHP_ROUND_HALF_UP);
                $payu_pos_id = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_pos_id')->getValue();
                $payu_pos_auth_key = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_pos_auth_key')->getValue();
                $payu_key_primary = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_key_primary')->getValue();
                $client_ip = $this->get('request')->getClientIp();

                $data = array();
                $data['url'] = 'https://secure.payu.com/paygw/UTF/NewPayment';
                $data['pos_id'] = $payu_pos_id;
                $data['pos_auth_key'] = $payu_pos_auth_key;
                $data['session_id'] = $entity->getPayuSessionId();
                $data['amount'] = $amount * 100;
                $data['desc'] = 'Opłata za zamówienie o ID ' . $entity->getId();
                $data['first_name'] = $entity->getCustomerFirstname();
                $data['last_name'] = $entity->getCustomerLastname();
                $data['email'] = $entity->getCustomerEmail();
                $data['client_ip'] = $client_ip;
                $data['ts'] = time();
                $data['key_1'] = $payu_key_primary;
                $data['sig'] = md5($data['pos_id'] . $data['session_id'] . $data['pos_auth_key'] . $data['amount'] . $data['desc'] . $data['first_name'] . $data['last_name'] . $data['email'] . $data['client_ip'] . $data['ts'] . $data['key_1']);

                return $this->render('LsBasketBundle:Front:payu.html.twig', array(
                    'data' => $data
                ));
            } else {
                return $this->render('LsBasketBundle:Front:payuAlreadyStarted.html.twig', array());
            }
        } else {
            return $this->render('LsBasketBundle:Front:payuNoOrder.html.twig', array());
        }
    }

    public function payuStartedAction() {
        $em = $this->getDoctrine()->getManager();

        $payu_session_id = $this->get('request')->get('session');

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.payu_session_id = :payu_session_id')
            ->setParameter('payu_session_id', $payu_session_id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity) {
            $entity->setPayuStartedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();
        }

        return new Response('OK');
    }

    public function thanksAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity) {
            switch ($entity->getPaymentForm()) {
                case Order::PAYMENT_FORM_POBRANIE:
                    return $this->render('LsBasketBundle:Front:thanksPobranie.html.twig', array());
                    break;
                case Order::PAYMENT_FORM_PRZELEW:
                    $total = number_format($entity->getTotalValue(), 2, ',', '') . ' zł';
                    return $this->render('LsBasketBundle:Front:thanksPrzelew.html.twig', array(
                        'total' => $total
                    ));
                    break;
                case Order::PAYMENT_FORM_PAYU:
                    $entity->setPayuSessionId(uniqid('payment-', true));
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('ls_basket_payu', array('payu_session_id' => $entity->getPayuSessionId())));
                    break;
            }
        }
    }

    public function cancelAction($guid) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.cancelation_guid = :guid')
            ->setParameter('guid', $guid)
            ->getQuery()
            ->getOneOrNullResult();

        $text = '';

        if (null !== $entity) {
            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówienie o ID <b>' . $entity->getId() . '</b> zostało anulowane.</p>';

            $em->remove($entity);
            $em->flush();

            $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_admin')->getValue();

            $message = \Swift_Message::newInstance();
            $message->setSubject('Anulowanie zamówienia w portalu regiorecords.pl');
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email_to);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $text .= 'Anulowanie zamówienia zakończone sukcesem.';
        } else {
            $text .= 'Z podanym identyfikatorem nie jest związane żadne zamówienie.';
        }

        return $this->render('LsBasketBundle:Front:cancel.html.twig', array(
            'message' => $text
        ));
    }

    public function confirmAction($guid) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.confirmation_guid = :guid')
            ->setParameter('guid', $guid)
            ->getQuery()
            ->getOneOrNullResult();

        $text = '';

        if (null !== $entity) {
            if ($entity->getStatus() < Order::STATUS_POTWIERDZONE) {
                $entity->setStatus(Order::STATUS_POTWIERDZONE);
                $entity->setConfirmationGuid(null);

                $em->persist($entity);
                $em->flush();

                $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówienie o ID <b>' . $entity->getId() . '</b> zostało potwierdzone.</p>';

                $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_admin')->getValue();

                $message = \Swift_Message::newInstance();
                $message->setSubject('Potwierdzenie zamówienia w portalu regiorecords.pl');
                $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
                $message->setTo($email_to);
                $message->setBody($message_txt, 'text/html');
                $message->addPart(strip_tags($message_txt), 'text/plain');

                $mailer = $this->get('mailer');
                $mailer->send($message);
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->container->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
            }
            $text .= 'Potwierdzenie złożenia zamówienia zakończone sukcesem.';
        } else {
            $text .= 'Z podanym identyfikatorem nie jest związane żadne zamówienie.';
        }

        return $this->render('LsBasketBundle:Front:confirm.html.twig', array(
            'message' => $text
        ));
    }
}
