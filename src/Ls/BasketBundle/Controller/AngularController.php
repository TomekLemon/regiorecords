<?php

namespace Ls\BasketBundle\Controller;

use Doctrine\ORM\Query;
use Ls\CoreBundle\Utils\Tools;
use Ls\OrderBundle\Entity\Order;
use Ls\OrderBundle\Entity\OrderProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Angular controller.
 *
 */
class AngularController extends Controller {

    public function productGetAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsProductBundle:Product', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            $item = null;
        } else {
            $item = array(
                'id' => $entity->getId(),
                'author' => $entity->getAuthor(),
                'title' => $entity->getTitle(),
                'price' => $entity->getPrice(),
                'quantity' => $entity->getQuantity(),
                'photoUrl' => $entity->getThumbWebPath('list'),
            );
        }

        return new JsonResponse($item);
    }

    public function productReloadAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request')->getContent();

        $post = json_decode($request);

        $qb = $em->createQueryBuilder();
        $result = $qb->select('p')
            ->from('LsProductBundle:Product', 'p')
            ->add('where', $qb->expr()->in('p.id', $post))
            ->getQuery()
            ->getResult();

        $items = array();
        foreach ($result as $entity) {
            $item = array(
                'id' => $entity->getId(),
                'author' => $entity->getAuthor(),
                'title' => $entity->getTitle(),
                'price' => $entity->getPrice(),
                'quantity' => $entity->getQuantity(),
                'photoUrl' => $entity->getThumbWebPath('list'),
            );

            $items[] = $item;
        }

        return new JsonResponse($items);
    }

    public function paymentFormsAction() {
        $em = $this->getDoctrine()->getManager();

        $cost0 = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_pobranie')->getValue();
        $cost0 = str_replace(',', '.', $cost0);
        $cost0 = number_format($cost0, 2, '.', '');
        $cost1 = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_przelew')->getValue();
        $cost1 = str_replace(',', '.', $cost1);
        $cost1 = number_format($cost1, 2, '.', '');
        $cost2 = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_payu')->getValue();
        $cost2 = str_replace(',', '.', $cost2);
        $cost2 = number_format($cost2, 2, '.', '');

        $item = array(
            array(
                'id' => 0,
                'name' => 'Za pobraniem',
                'cost' => $cost0
            ),
            array(
                'id' => 1,
                'name' => 'Przelew',
                'cost' => $cost1
            ),
            array(
                'id' => 2,
                'name' => 'PayU',
                'cost' => $cost2
            )
        );

        return new JsonResponse($item);
    }

    public function orderSendAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request')->getContent();

        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = null;
        } else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        $post = json_decode($request);

        $order = new Order();

        if (null !== $user) {
            $order->setUser($user);
        }

        $cost = 0.0;
        $send_confirm = true;
        switch ($post->payment_form) {
            case Order::PAYMENT_FORM_POBRANIE:
                $cost = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_pobranie')->getValue();
                $cost = str_replace(',', '.', $cost);
                $cost = floatval(number_format($cost, 2, '.', ''));
                break;
            case Order::PAYMENT_FORM_PRZELEW:
                $cost = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_przelew')->getValue();
                $cost = str_replace(',', '.', $cost);
                $cost = floatval(number_format($cost, 2, '.', ''));
                break;
            case Order::PAYMENT_FORM_PAYU:
                $cost = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('delivery_cost_payu')->getValue();
                $cost = str_replace(',', '.', $cost);
                $cost = floatval(number_format($cost, 2, '.', ''));
                $send_confirm = false;
                break;
        }

        $order->setStatus(Order::STATUS_NOWE);
        $order->setDeliveryCost($cost);
        $order->setPaymentForm($post->payment_form);

        $order->setCustomerFirstname($post->customer->firstname);
        $order->setCustomerLastname($post->customer->lastname);
        $order->setCustomerPhone($post->customer->phone);
        $order->setCustomerEmail($post->customer->email);
        $order->setCustomerStreet($post->customer->street);
        $order->setCustomerZip($post->customer->zip);
        $order->setCustomerCity($post->customer->city);

        if ($send_confirm) {
            $order->setConfirmationGuid(Tools::generateGuid());
        }

        $em->persist($order);
        $em->flush();

        $totalOrder = 0.0;
        foreach ($post->items as $item) {
            $product = $em->getRepository('LsProductBundle:Product')->findOneById($item->id);
            $quantity = floatval($item->quantity);
            $price = floatval($product->getPrice());
            $total = $quantity * $price;
            $totalOrder += $total;

            $order_item = new OrderProduct();
            $order_item->setOrder($order);
            $order_item->setProduct($product);
            $order_item->setQuantity($quantity);
            $order_item->setPrice($price);
            $order_item->setTotal($total);

            $em->persist($order_item);
            $em->flush();

            $order->addOrderProduct($order_item);
        }

        $totalOrder += $cost;

        $order->setTotalValue($totalOrder);
        $em->persist($order);
        $em->flush();

        /* E-mail do administratora */
        $url = $this->get('router')->generate('ls_core_admin', array(), true);
        $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Złożno nowe zamówienie w portalu regiorecords.pl. ID zamówienia to <b>' . $order->getId() . '</b>. Szczegóły zamówienia do wglądu w <a href="' . $url . '">panelu administracyjnym</a> portalu.</p>';

        $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_admin')->getValue();

        $message = \Swift_Message::newInstance();
        $message->setSubject('Nowe zamówienie w portalu regiorecords.pl');
        $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
        $message->setTo($email_to);
        $message->setBody($message_txt, 'text/html');
        $message->addPart(strip_tags($message_txt), 'text/plain');

        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->container->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);

        /* E-mail do składającego zamówienie */
        $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Witaj ' . $order->getCustomerFirstname() . ',</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Złożyłeś zamówienie w sklepie RegioRecords. ID zamówienia: ' . $order->getId() . '</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Forma płatności: <b>' . $order->getPaymentFormName() . '</b></p>';
        if ($order->getPaymentForm() == Order::PAYMENT_FORM_PRZELEW) {
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Prosimy o wpłatę pieniędzy <span>(' . number_format($order->getTotalValue(), 2, ',', '') . ' zł)</span> na konto:</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_name')->getValue() . '</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_street')->getValue() . '</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_postal')->getValue() . ' ' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_city')->getValue() . '</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Nr konta bankowego: ' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('bank_account')->getValue() . '</p>';
        }
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Adres wysyłki:</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><b>' . $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname() . '</b></p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><b>' . $order->getCustomerStreet() . '</b></p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><b>' . $order->getCustomerZip() . ' ' . $order->getCustomerCity() . '</b></p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówione produkty to:</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<table style="border-collapse: collapse; font-size: 12px; line-height: 22px;">';
        $message_txt .= '<tr>';
        $message_txt .= '<th style="border: 1px solid #000000; text-align: center; padding: 0 20px;">Nazwa</th>';
        $message_txt .= '<th style="border: 1px solid #000000; text-align: center; padding: 0 20px;">Cena jednostkowa</th>';
        $message_txt .= '<th style="border: 1px solid #000000; text-align: center; padding: 0 20px;">Ilość</th>';
        $message_txt .= '<th style="border: 1px solid #000000; text-align: center; padding: 0 20px;">Razem</th>';
        $message_txt .= '<tr>';
        foreach ($order->getOrderProducts() as $product) {
            $message_txt .= '<tr>';
            $message_txt .= '<td style="border: 1px solid #000000; padding: 0 20px;">' . $product->getProduct()->getAuthor() . ' - ' . $product->getProduct()->getTitle() . '</td>';
            $message_txt .= '<td style="border: 1px solid #000000; text-align: right; padding: 0 20px;">' . number_format($product->getPrice(), 2, ',', '') . ' zł</td>';
            $message_txt .= '<td style="border: 1px solid #000000; text-align: right; padding: 0 20px;">' . $product->getQuantity() . ' szt.</td>';
            $message_txt .= '<td style="border: 1px solid #000000; text-align: right; padding: 0 20px;">' . number_format($product->getTotal(), 2, ',', '') . ' zł</td>';
            $message_txt .= '</tr>';
        }
        $message_txt .= '<tr>';
        $message_txt .= '<td colspan="3" style="border: 1px solid #000000; text-align: right; padding-right: 20px;">Koszty dostawy</td>';
        $message_txt .= '<td style="border: 1px solid #000000; text-align: right; padding: 0 20px;">' . number_format($order->getDeliveryCost(), 2, ',', '') . ' zł</td>';
        $message_txt .= '<tr>';
        $message_txt .= '<tr>';
        $message_txt .= '<td colspan="3" style="border: 1px solid #000000; text-align: right; padding-right: 20px;">Razem</td>';
        $message_txt .= '<td style="border: 1px solid #000000; text-align: right; padding: 0 20px;">' . number_format($order->getTotalValue(), 2, ',', '') . ' zł</td>';
        $message_txt .= '<tr>';
        $message_txt .= '</table>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        if ($send_confirm) {
            $url = $this->get('router')->generate('ls_basket_confirm', array('guid' => $order->getConfirmationGuid()), true);
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Aby potwierdzić zamówienie kliknij w link znajdujący się poniżej:</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><a href="' . $url . '">Potwierdz zamówienie</a></p>';
        }
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">pozdrawiamy i życzymy udanych zakupów,</p>';
        $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zespół Regio Records</p>';

        $email_to = $order->getCustomerEmail();

        $message = \Swift_Message::newInstance();
        $message->setSubject('Zamówienie ID ' . $order->getId() . ' w portalu regiorecords.pl');
        $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
        $message->setTo($email_to);
        $message->setBody($message_txt, 'text/html');
        $message->addPart(strip_tags($message_txt), 'text/plain');

        $mailer = $this->get('mailer');
        $mailer->send($message);
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->container->get('swiftmailer.transport.real');
        $spool->flushQueue($transport);

        $response = array(
            'url' => $this->get('router')->generate('ls_basket_thanks', array('id' => $order->getId()), true)
        );

        return new JsonResponse($response);
    }

    public function userAuthenticatedGetAction() {
        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $status = 404;

            $item = null;
        } else {
            $entity = $this->get('security.token_storage')->getToken()->getUser();
            $status = 200;

            $item = array(
                'id' => $entity->getId(),
                'firstname' => $entity->getFirstname(),
                'lastname' => $entity->getLastname(),
                'email' => $entity->getEmail(),
                'phone' => $entity->getPhone(),
                'street' => $entity->getStreet(),
                'zip' => $entity->getZip(),
                'city' => $entity->getCity(),
            );
        }

        return new JsonResponse($item, $status);
    }
}
