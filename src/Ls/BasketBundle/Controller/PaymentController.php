<?php

namespace Ls\BasketBundle\Controller;

use Ls\CoreBundle\Utils\Tools;
use Ls\OrderBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Basket controller.
 *
 */
class PaymentController extends Controller {

    public function errorAction() {
        $em = $this->getDoctrine()->getManager();

        $payu_session_id = $this->get('request')->get('session');
        $error = $this->get('request')->get('error');

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.payu_session_id = :payu_session_id')
            ->setParameter('payu_session_id', $payu_session_id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity && $entity->getStatus() < Order::STATUS_OCZEKUJE_NA_PLATNOSC) {
            $entity->setStatus(Order::STATUS_OCZEKUJE_NA_PLATNOSC);
            $entity->setPayuStatus(1);
            $entity->setPayuError($error);
            $em->persist($entity);
            $em->flush();
        }

        return $this->render('LsBasketBundle:Payment:error.html.twig', array(
            'error' => Order::$payu_errors_text[$error]
        ));
    }

    public function successAction() {
        $em = $this->getDoctrine()->getManager();

        $payu_session_id = $this->get('request')->get('session');

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOrderBundle:Order', 'p')
            ->where('p.payu_session_id = :payu_session_id')
            ->setParameter('payu_session_id', $payu_session_id)
            ->getQuery()
            ->getOneOrNullResult();

        if (null !== $entity && $entity->getStatus() < Order::STATUS_OCZEKUJE_NA_PLATNOSC) {
            $entity->setStatus(Order::STATUS_OCZEKUJE_NA_PLATNOSC);
            $entity->setPayuStatus(1);
            $em->persist($entity);
            $em->flush();
        }

        return $this->render('LsBasketBundle:Payment:success.html.twig', array());
    }

    public function raportAction() {
        $em = $this->getDoctrine()->getManager();

        $pos_id = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_pos_id')->getValue();
        $pos_auth_key = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_pos_auth_key')->getValue();
        $key_primary = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_key_primary')->getValue();
        $key_secondary = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('payu_key_secondary')->getValue();

        $resp_pos_id = $this->get('request')->request->get('pos_id');
        $resp_session_id = $this->get('request')->request->get('session_id');
        $resp_ts = $this->get('request')->request->get('ts');
        $resp_sig = $this->get('request')->request->get('sig');

        if ((md5($resp_pos_id . $resp_session_id . $resp_ts . $key_secondary) == $resp_sig)) {
            $entity = $em->createQueryBuilder()
                ->select('p')
                ->from('LsOrderBundle:Order', 'p')
                ->where('p.payu_session_id = :payu_session_id')
                ->setParameter('payu_session_id', $resp_session_id)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $entity) {
                $server = 'secure.payu.com';
                $server_script = '/paygw/UTF/Payment/get/xml';
                $ts = time();
                $sig = md5($pos_id . $resp_session_id . $ts . $key_primary);
                $parameters = "pos_id=" . $pos_id . "&session_id=" . $resp_session_id . "&ts=" . $ts . "&sig=" . $sig;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://" . $server . $server_script);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $tresc = curl_exec($ch);

                $xml_data = simplexml_load_string($tresc);

                if (md5($xml_data->trans->pos_id . $xml_data->trans->session_id . $xml_data->trans->order_id . $xml_data->trans->status . $xml_data->trans->amount . $xml_data->trans->desc . $xml_data->trans->ts . $key_secondary) == $xml_data->trans->sig) {
                    switch ($xml_data->trans->status) {
                        case '2':
                            $entity->setCancelationGuid(Tools::generateGuid());
                            $entity->setStatus(Order::STATUS_OCZEKUJE_NA_PLATNOSC);
                            $entity->setPaymentForm(Order::PAYMENT_FORM_PRZELEW);
                            $entity->setPayuStatus($xml_data->trans->status);
                            $em->persist($entity);
                            $em->flush();

                            $url = $this->get('router')->generate('ls_basket_cancel', array('guid' => $entity->getCancelationGuid()), true);

                            /* E-mail do administratora */
                            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówienie o ID <b>' . $entity->getId() . '</b> zmieniło status na "' . $entity->getStatusName() . '".</p>';

                            $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_admin')->getValue();

                            $message = \Swift_Message::newInstance();
                            $message->setSubject('Zmiana statusu zamówienia w portalu regiorecords.pl');
                            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
                            $message->setTo($email_to);
                            $message->setBody($message_txt, 'text/html');
                            $message->addPart(strip_tags($message_txt), 'text/plain');

                            $mailer = $this->get('mailer');
                            $mailer->send($message);
                            $spool = $mailer->getTransport()->getSpool();
                            $transport = $this->container->get('swiftmailer.transport.real');
                            $spool->flushQueue($transport);

                            /* E-mail do klienta */
                            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Witaj ' . $entity->getCustomerFirstname() . ',</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Wystąpił błąd podczas płatności PayU dla zamówienia o ID: ' . $entity->getId() . '</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Jeśli nadal chcesz kontynuować zamówienie prosimy o wpłatę pieniędzy <span>(' . number_format($entity->getTotalValue(), 2, ',', '') . ' zł)</span> na konto:</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_name')->getValue() . '</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_street')->getValue() . '</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_postal')->getValue() . ' ' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('contact_city')->getValue() . '</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Nr konta bankowego: ' . $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('bank_account')->getValue() . '</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Aby anulować zamówienie kliknij w link znajdujący się poniżej:</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;"><a href="' . $url . '">Anuluj zamówienie</a></p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">pozdrawiamy i życzymy udanych zakupów,</p>';
                            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zespół Regio Records</p>';

                            $email_to = $entity->getCustomerEmail();

                            $message = \Swift_Message::newInstance();
                            $message->setSubject('Zamówienie ID ' . $entity->getId() . ' w portalu regiorecords.pl - błąd płatności PayU');
                            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
                            $message->setTo($email_to);
                            $message->setBody($message_txt, 'text/html');
                            $message->addPart(strip_tags($message_txt), 'text/plain');

                            $mailer = $this->get('mailer');
                            $mailer->send($message);
                            $spool = $mailer->getTransport()->getSpool();
                            $transport = $this->container->get('swiftmailer.transport.real');
                            $spool->flushQueue($transport);
                            break;
                        case '3':
                        case '4':
                        case '5':
                        case '7':
                        case '888':
                            $entity->setPayuStatus($xml_data->trans->status);
                            $em->persist($entity);
                            $em->flush();

                            break;
                        case '99':
                            $entity->setStatus(Order::STATUS_W_REALIZACJI);
                            $entity->setPayuStatus($xml_data->trans->status);
                            $em->persist($entity);
                            $em->flush();

                            /* E-mail do administratora */
                            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówienie o ID <b>' . $entity->getId() . '</b> zmieniło status na "' . $entity->getStatusName() . '".</p>';

                            $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_admin')->getValue();

                            $message = \Swift_Message::newInstance();
                            $message->setSubject('Zmiana statusu zamówienia w portalu regiorecords.pl');
                            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
                            $message->setTo($email_to);
                            $message->setBody($message_txt, 'text/html');
                            $message->addPart(strip_tags($message_txt), 'text/plain');

                            $mailer = $this->get('mailer');
                            $mailer->send($message);
                            $spool = $mailer->getTransport()->getSpool();
                            $transport = $this->container->get('swiftmailer.transport.real');
                            $spool->flushQueue($transport);
                            break;
                    }
                }
            }
        }

        return new Response('OK');
    }
}
