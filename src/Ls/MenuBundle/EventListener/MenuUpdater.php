<?php

namespace Ls\MenuBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\MenuBundle\Entity\MenuItem;

class MenuUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postRemove',
        );
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof MenuItem) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $location = $entity->getLocation();
                $parent = $entity->getParent();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMenuBundle:MenuItem', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.location = :location')
                    ->andWhere('c.parent = :parent')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('location', $location)
                    ->setParameter('parent', $parent)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsMenuBundle:MenuItem';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }
}