<?php

namespace Ls\OrderBundle\Form;

use Ls\OrderBundle\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderStatusType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('status', 'choice', array(
            'choices' => Order::getStatusNames(),
            'label' => 'Status',
            'expanded' => true,
            'multiple' => false,
        ));
        $builder->add('submit', 'submit', array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OrderBundle\Entity\Order',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_admin_order_status';
    }
}
