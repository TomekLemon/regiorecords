<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Order
 * @ORM\Table(name="order_item")
 * @ORM\Entity
 */
class Order {
    const PAYMENT_FORM_POBRANIE       = 0;
    const PAYMENT_FORM_PRZELEW        = 1;
    const PAYMENT_FORM_PAYU           = 2;
    const STATUS_NOWE                 = 0;
    const STATUS_POTWIERDZONE         = 1;
    const STATUS_OCZEKUJE_NA_PLATNOSC = 2;
    const STATUS_W_REALIZACJI         = 3;
    const STATUS_ZREALIZOWANE         = 4;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_zip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $customer_city;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $delivery_cost;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $payment_form;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $payu_session_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $payu_status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $payu_error;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $payu_started_at;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $total_value;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $cancelation_guid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $confirmation_guid;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\UserBundle\Entity\User",
     *     inversedBy="orders"
     * )
     * @ORM\JoinColumn(
     *     name="user_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\UserBundle\Entity\User
     */
    private $user;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\OrderBundle\Entity\OrderProduct",
     *   mappedBy="order"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $order_products;

    static $payu_status_text = array(
        1 => 'nowa',
        2 => 'anulowana',
        3 => 'odrzucona',
        4 => 'rozpoczęta',
        5 => 'oczekuje na odbiór',
        7 => 'płatność zwrócona, otrzymano środki od klienta po wcześniejszym anulowaniu transakcji, lub nie było możliwości zwrotu środków w sposób automatyczny, sytuacje takie będą monitorowane i wyjaśniane przez zespół PayU',
        99 => 'płatność odebrana - zakończona',
        888 => 'błędny status - prosimy o kontakt',
    );

    static $payu_status_text_short = array(
        1 => 'nowa',
        2 => 'anulowana',
        3 => 'odrzucona',
        4 => 'rozpoczęta',
        5 => 'oczekuje na odbiór',
        7 => 'zwrócona',
        99 => 'zakończona',
        888 => 'błędny status',
    );

    static $payu_errors_text = array(
        100 => 'Brak lub błędna wartość parametru pos_id',
        101 => 'Brak parametru session_id',
        102 => 'Brak parametru ts',
        103 => 'Brak lub błędna wartość parametru sig',
        104 => 'Brak parametru desc',
        105 => 'Brak parametru client_ip',
        106 => 'Brak parametru first_name',
        107 => 'Brak parametru last_name',
        108 => 'Brak parametru street',
        109 => 'Brak parametru city',
        110 => 'Brak parametru post_code',
        111 => 'Brak parametru amount (lub/oraz amount_netto dla usługi SMS)',
        112 => 'Błędny numer konta bankowego',
        113 => 'Brak parametru email',
        114 => 'Brak numeru telefonu',
        200 => 'Inny chwilowy błąd',
        201 => 'Inny chwilowy błąd bazy danych',
        202 => 'POS o podanym identyfikatorze jest zablokowany',
        203 => 'Niedozwolona wartość pay_type dla danego parametru pos_id',
        204 => 'Podana metoda płatności (wartość pay_type) jest chwilowo zablokowana dla danego parametru pos_id, np. przerwa konserwacyjna bramki płatniczej',
        205 => 'Kwota transakcji mniejsza od wartości minimalnej',
        206 => 'Kwota transakcji większa od wartości maksymalnej',
        207 => 'Przekroczona wartość wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym',
        208 => 'POS działa w wariancie ExpressPayment lecz nie nastąpiła aktywacja tego wariantu współpracy (czekamy na zgodę działu obsługi klienta)',
        209 => 'Błędny numer pos_id lub pos_auth_key',
        211 => 'Nieprawidłowa waluta transakcji',
        212 => 'Próba utworzenia transakcji częściej niż raz na minutę - dla nieaktywnej firmy',
        500 => 'Transakcja nie istnieje',
        501 => 'Brak autoryzacji dla danej transakcji',
        502 => 'Transakcja rozpoczęta wcześniej',
        503 => 'Autoryzacja do transakcji była już przeprowadzana',
        504 => 'Transakcja anulowana wcześniej',
        505 => 'Transakcja przekazana do odbioru wcześniej',
        506 => 'Transakcja już odebrana',
        507 => 'Błąd podczas zwrotu środków do Klienta',
        508 => 'Klient zrezygnował z płatności',
        599 => 'Błędny stan transakcji, np. nie można uznać transakcji kilka razy lub inny, prosimy o kontakt',
        999 => 'Inny błąd krytyczny - prosimy o kontakt',
    );

    public function __construct() {
        $this->created_at = new \DateTime();
        $this->order_products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set customer_firstname
     *
     * @param string $customer_firstname
     * @return Order
     */
    public function setCustomerFirstname($customer_firstname) {
        $this->customer_firstname = $customer_firstname;
        return $this;
    }

    /**
     * Get customer_firstname
     *
     * @return string
     */
    public function getCustomerFirstname() {
        return $this->customer_firstname;
    }

    /**
     * Set customer_lastname
     *
     * @param string $customer_lastname
     * @return Order
     */
    public function setCustomerLastname($customer_lastname) {
        $this->customer_lastname = $customer_lastname;
        return $this;
    }

    /**
     * Get customer_lastname
     *
     * @return string
     */
    public function getCustomerLastname() {
        return $this->customer_lastname;
    }

    /**
     * Set customer_phone
     *
     * @param string $customer_phone
     * @return Order
     */
    public function setCustomerPhone($customer_phone) {
        $this->customer_phone = $customer_phone;
        return $this;
    }

    /**
     * Get customer_phone
     *
     * @return string
     */
    public function getCustomerPhone() {
        return $this->customer_phone;
    }

    /**
     * Set customer_email
     *
     * @param string $customer_email
     * @return Order
     */
    public function setCustomerEmail($customer_email) {
        $this->customer_email = $customer_email;
        return $this;
    }

    /**
     * Get customer_email
     *
     * @return string
     */
    public function getCustomerEmail() {
        return $this->customer_email;
    }

    /**
     * Set customer_street
     *
     * @param string $customer_street
     * @return Order
     */
    public function setCustomerStreet($customer_street) {
        $this->customer_street = $customer_street;
        return $this;
    }

    /**
     * Get customer_street
     *
     * @return string
     */
    public function getCustomerStreet() {
        return $this->customer_street;
    }

    /**
     * Set customer_zip
     *
     * @param string $customer_zip
     * @return Order
     */
    public function setCustomerZip($customer_zip) {
        $this->customer_zip = $customer_zip;
        return $this;
    }

    /**
     * Get customer_zip
     *
     * @return string
     */
    public function getCustomerZip() {
        return $this->customer_zip;
    }

    /**
     * Set customer_city
     *
     * @param string $customer_city
     * @return Order
     */
    public function setCustomerCity($customer_city) {
        $this->customer_city = $customer_city;
        return $this;
    }

    /**
     * Get customer_city
     *
     * @return string
     */
    public function getCustomerCity() {
        return $this->customer_city;
    }

    /**
     * Set delivery_cost
     *
     * @param string $delivery_cost
     * @return Order
     */
    public function setDeliveryCost($delivery_cost) {
        $this->delivery_cost = $delivery_cost;

        return $this;
    }

    /**
     * Get delivery_cost
     *
     * @return string
     */
    public function getDeliveryCost() {
        return $this->delivery_cost;
    }

    /**
     * Set payment_form
     *
     * @param integer $payment_form
     * @return Order
     */
    public function setPaymentForm($payment_form) {
        $this->payment_form = $payment_form;
        return $this;
    }

    /**
     * Get payment_form
     *
     * @return integer
     */
    public function getPaymentForm() {
        return $this->payment_form;
    }

    /**
     * Get payment_form_name
     *
     * @return string
     */
    public function getPaymentFormName() {
        $name = '';

        switch ($this->payment_form) {
            case Order::PAYMENT_FORM_POBRANIE:
                $name = 'Za pobraniem';
                break;
            case Order::PAYMENT_FORM_PRZELEW:
                $name = 'Przelew';
                break;
            case Order::PAYMENT_FORM_PAYU:
                $name = 'PayU';
                break;
        }

        return $name;
    }

    /**
     * Set payu_session_id
     *
     * @param string $payu_session_id
     * @return Order
     */
    public function setPayuSessionId($payu_session_id) {
        $this->payu_session_id = $payu_session_id;

        return $this;
    }

    /**
     * Get payu_session_id
     *
     * @return string
     */
    public function getPayuSessionId() {
        return $this->payu_session_id;
    }

    /**
     * Set payu_status
     *
     * @param integer $payu_status
     * @return Order
     */
    public function setPayuStatus($payu_status) {
        $this->payu_status = $payu_status;

        return $this;
    }

    /**
     * Get payu_status
     *
     * @return integer
     */
    public function getPayuStatus() {
        return $this->payu_status;
    }

    /**
     * Get payu_status_name
     *
     * @return string
     */
    public function getPayuStatusName() {
        return Order::$payu_status_text_short[$this->payu_status];
    }

    /**
     * Set payu_error
     *
     * @param integer $payu_error
     * @return Order
     */
    public function setPayuError($payu_error) {
        $this->payu_error = $payu_error;

        return $this;
    }

    /**
     * Get payu_error
     *
     * @return integer
     */
    public function getPayuError() {
        return $this->payu_error;
    }

    /**
     * Set payu_started_at
     *
     * @param \DateTime $payu_started_at
     * @return Order
     */
    public function setPayuStartedAt($payu_started_at) {
        $this->payu_started_at = $payu_started_at;

        return $this;
    }

    /**
     * Get payu_started_at
     *
     * @return \DateTime
     */
    public function getPayuStartedAt() {
        return $this->payu_started_at;
    }

    /**
     * Set total_value
     *
     * @param string $total_value
     * @return Order
     */
    public function setTotalValue($total_value) {
        $this->total_value = $total_value;

        return $this;
    }

    /**
     * Get total_value
     *
     * @return string
     */
    public function getTotalValue() {
        return $this->total_value;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Order
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Get payment_form_name
     *
     * @return string
     */
    public function getStatusName() {
        $name = '';

        switch ($this->status) {
            case Order::STATUS_NOWE:
                $name = 'Nowe';
                break;
            case Order::STATUS_POTWIERDZONE:
                $name = 'Potwierdzone';
                break;
            case Order::STATUS_OCZEKUJE_NA_PLATNOSC:
                $name = 'Oczekuje na płatność';
                break;
            case Order::STATUS_W_REALIZACJI:
                $name = 'W realizacji';
                break;
            case Order::STATUS_ZREALIZOWANE:
                $name = 'Zrealizowane';
                break;
        }

        return $name;
    }

    /**
     * Get payment_form_name
     *
     * @return string
     */
    public function getStatusClass() {
        $class = '';

        switch ($this->status) {
            case Order::STATUS_NOWE:
                $class = 'Orders-item-status-new';
                break;
            case Order::STATUS_POTWIERDZONE:
                $class = 'Orders-item-status-confirmed';
                break;
            case Order::STATUS_OCZEKUJE_NA_PLATNOSC:
                $class = 'Orders-item-status-waiting';
                break;
            case Order::STATUS_W_REALIZACJI:
                $class = 'Orders-item-status-in-progress';
                break;
            case Order::STATUS_ZREALIZOWANE:
                $class = 'Orders-item-status-realised';
                break;
        }

        return $class;
    }

    /**
     * Get payment_form_name
     *
     * @return string
     */
    public static function getStatusNames() {
        $names = array(
            Order::STATUS_NOWE => 'Nowe',
            Order::STATUS_POTWIERDZONE => 'Potwierdzone',
            Order::STATUS_OCZEKUJE_NA_PLATNOSC => 'Oczekuje na płatność',
            Order::STATUS_W_REALIZACJI => 'W realizacji',
            Order::STATUS_ZREALIZOWANE => 'Zrealizowane'
        );

        return $names;
    }

    /**
     * Set cancelation_guid
     *
     * @param string $cancelation_guid
     * @return Order
     */
    public function setCancelationGuid($cancelation_guid) {
        $this->cancelation_guid = $cancelation_guid;
        return $this;
    }

    /**
     * Get cancelation_guid
     *
     * @return string
     */
    public function getCancelationGuid() {
        return $this->cancelation_guid;
    }

    /**
     * Set confirmation_guid
     *
     * @param string $confirmation_guid
     * @return Order
     */
    public function setConfirmationGuid($confirmation_guid) {
        $this->confirmation_guid = $confirmation_guid;
        return $this;
    }

    /**
     * Get confirmation_guid
     *
     * @return string
     */
    public function getConfirmationGuid() {
        return $this->confirmation_guid;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt($createdAt = null) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Order
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set user
     *
     * @param \Ls\UserBundle\Entity\User $user
     * @return Order
     */
    public function setUser(User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ls\UserBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add order_product
     *
     * @param \Ls\OrderBundle\Entity\OrderProduct $order_product
     * @return Order
     */
    public function addOrderProduct(OrderProduct $order_product) {
        $order_product->setOrder($this);
        $this->order_products[] = $order_product;

        return $this;
    }

    /**
     * Remove order_product
     *
     * @param \Ls\OrderBundle\Entity\OrderProduct $order_product
     */
    public function removeOrderProduct(OrderProduct $order_product) {
        $this->order_products->removeElement($order_product);
    }

    /**
     * Get order_products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderProducts() {
        return $this->order_products;
    }

    public function __toString() {
        if (is_null($this->getId())) {
            return 'NULL';
        }
        return $this->getId();
    }
}