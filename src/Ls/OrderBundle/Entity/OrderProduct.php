<?php

namespace Ls\OrderBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\ProductBundle\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderProduct
 * @ORM\Table(name="order_product")
 * @ORM\Entity
 */
class OrderProduct {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $total;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Order",
     *     inversedBy="order_products"
     * )
     * @ORM\JoinColumn(
     *     name="order_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\OrderBundle\Entity\Order
     */
    private $order;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\ProductBundle\Entity\Product",
     *     inversedBy="order_products"
     * )
     * @ORM\JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\ProductBundle\Entity\Product
     */
    private $product;

    public function __construct() {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return OrderProduct
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return OrderProduct
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return OrderProduct
     */
    public function setTotal($total) {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal() {
        return $this->total;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return OrderProduct
     */
    public function setCreatedAt($createdAt = null) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return OrderProduct
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set order
     *
     * @param \Ls\OrderBundle\Entity\Order $order
     * @return OrderProduct
     */
    public function setOrder(Order $order = null) {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Ls\OrderBundle\Entity\Order
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \Ls\ProductBundle\Entity\Product $product
     * @return OrderProduct
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Ls\ProductBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    public function __toString() {
        if (is_null($this->getProduct()->getSeoTitle())) {
            return 'NULL';
        }
        return $this->getProduct()->getSeoTitle();
    }
}