<?php

namespace Ls\OrderBundle\Controller;

use Ls\OrderBundle\Form\OrderStatusType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_order_pager_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOrderBundle:Order', 'e')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_order'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Produkty', $this->get('router')->generate('ls_admin_order'));

        return $this->render('LsOrderBundle:Admin:index.html.twig', array(
            'entities' => $entities,
            'limit' => $limit,
            'page' => $page,
        ));
    }

    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOrderBundle:Order')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Produkty', $this->get('router')->generate('ls_admin_order'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_order_show', array('id' => $entity->getId())));

        return $this->render('LsOrderBundle:Admin:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    public function statusAction($id) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $response = array(
            'status' => 'ERROR',
            'html' => '',
            'errors' => array()
        );

        $entity = $em->getRepository('LsOrderBundle:Order')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $form = $this->createForm(new OrderStatusType(), $entity, array(
            'action' => $this->container->get('router')->generate('ls_admin_order_status', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $message_txt = '<p style="line-height: 18px; font-size: 12px; margin: 0;">Witaj ' . $entity->getCustomerFirstname() . ',</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zamówieniu o ID ' . $entity->getId() . ' w sklepie RegioRecords zmieniono status na: ' . $entity->getStatusName() . '</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">&nbsp;</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">pozdrawiamy i życzymy udanych zakupów,</p>';
            $message_txt .= '<p style="line-height: 18px; font-size: 12px; margin: 0;">Zespół Regio Records</p>';

            $email_to = $entity->getCustomerEmail();

            $message = \Swift_Message::newInstance();
            $message->setSubject('Zmiana statusu zamówienia o ID ' . $entity->getId());
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email_to);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $response['status'] = 'OK';
        }

        $response['html'] = iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsOrderBundle:Admin:status.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity
        ))->getContent());

        return new JsonResponse($response);
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
