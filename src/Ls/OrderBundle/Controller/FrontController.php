<?php

namespace Ls\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Order controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Order entity.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $user = null;

        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('ls_core_homepage'));
        } else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        $limit = 6;
        $allow = 1;

        $entities = $em->createQueryBuilder()
            ->select('o')
            ->from('LsOrderBundle:Order', 'o')
            ->where('o.user = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('o.created_at', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsOrderBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }

    public function moreAction() {
        $em = $this->getDoctrine()->getManager();

        $page = $this->get('request')->request->get('page');

        $limit = 6;
        $start = ($page * $limit);
        $allow = 1;
        $user = null;
        $response = array(
            'status' => 'ERROR',
            'allow' => $allow,
            'html' => ''
        );

        $securityContext = $this->get('security.context');
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') && !$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new JsonResponse($response);
        } else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
        }

        $entities = $em->createQueryBuilder()
            ->select('o')
            ->from('LsOrderBundle:Order', 'o')
            ->where('o.user = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('o.created_at', 'desc')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'status' => 'OK',
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsOrderBundle:Front:orders.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

}
