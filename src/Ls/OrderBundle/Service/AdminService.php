<?php

namespace Ls\OrderBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $menu, ItemInterface $parent, $route, $set) {
        $router = $this->container->get('router');

        $parent->addChild('Zamówienia', array(
            'route' => 'ls_admin_order',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_order':
                case 'ls_admin_order_show':
                    $menu->setCurrentUri($router->generate('ls_admin_order'));
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Zamówienia');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_order')));
    }
}

