$(document).ready(function () {
    var userDropdownVisible = false;

    activate();

    function activate() {
        handleUserDropdown();
        handleChangePassword();
        handleMenuMobile();
    }

    ///////////

    /**
     *  Handle opening and closing user dropdown
     */
    function handleUserDropdown() {
        var btnUser = '.logged-user';
        var dropdown = '.user-menu';

        $(btnUser).click(function (e) {
            e.preventDefault();

            if (userDropdownVisible) {
                $(dropdown).hide();
                $(btnUser).removeClass('whiteFill');
                userDropdownVisible = false;
            } else {
                $(dropdown).show();
                $(btnUser).addClass('whiteFill');
                userDropdownVisible = true;
            }
        });
    }

    /**
     *  Handle opening password change modal
     */
    function handleChangePassword() {
        $(".change-password-btn").leanModal({
            overlay: 0.9
        });
    }

    function handleMenuMobile() {
        var menu = $('#menu-mobile');
        $('#show-hide-menu').on('click', function () {
            if (menu.hasClass('visible')) {
                menu.removeClass('visible');
                menu.stop(true, true).slideUp(500, function () {
                });
            } else {
                menu.stop(true, true).slideDown(500, function () {
                    menu.addClass('visible');
                });
            }
        });
    }
});
