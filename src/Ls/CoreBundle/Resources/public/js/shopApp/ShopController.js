/**
 * Created by rafalglazar on 21.12.2015.
 */

(function () {
    'use strict';

    angular
        .module('ShopApplication')
        .controller('ShopController', ShopController);

    ShopController.$inject = ['$sessionStorage', 'ShopService'];

    function ShopController($sessionStorage, ShopService) {
        var vm = this;

        vm.customer = {
            firstname: '',
            lastname: '',
            phone: '',
            email: '',
            street: '',
            zip: '',
            city: ''
        };
        vm.deliveryCost = 0;
        vm.deliveryCostString = number_format(vm.deliveryCost, 2, ',', '') + ' zł';
        vm.deliveryFormString = '';
        vm.items = [];
        vm.paymentForm = 0;
        vm.paymentFormNames = [];
        vm.total = 0;
        vm.totalString = number_format(vm.total, 2, ',', '') + ' zł';
        vm.totalWithDelivery = 0;
        vm.totalWithDeliveryString = number_format(vm.totalWithDelivery, 2, ',', '') + ' zł';
        vm.userExists = false;
        vm.userDataError = '';

        vm.setup = setup;
        vm.updateTotal = updateTotal;
        vm.updatePaymentForm = updatePaymentForm;
        vm.addToBasket = addToBasket;
        vm.removeFromBasket = removeFromBasket;
        vm.checkQuantity = checkQuantity;
        vm.checkUserData = checkUserData;
        vm.storeCustomer = storeCustomer;
        vm.updateCustomer = updateCustomer;
        vm.sendOrder = sendOrder;

        ShopService.getUser()
            .then(function (data) {
                vm.userExists = true;
                if (data.firstname != null) {
                    vm.customer.firstname = data.firstname.trim();
                }
                if (data.lastname != null) {
                    vm.customer.lastname = data.lastname.trim();
                }
                if (data.phone != null) {
                    vm.customer.phone = data.phone.trim();
                }
                if (data.email != null) {
                    vm.customer.email = data.email.trim();
                }
                if (data.street != null) {
                    vm.customer.street = data.street.trim();
                }
                if (data.zip != null) {
                    vm.customer.zip = data.zip.trim();
                }
                if (data.city != null) {
                    vm.customer.city = data.city.trim();
                }
                $sessionStorage.basketCustomer = vm.customer;


                if (typeof $sessionStorage.basketUserId != 'undefined') {
                    if ($sessionStorage.basketUserId != null && $sessionStorage.basketUserId !== data.id) {
                        $sessionStorage.$reset();
                    }
                    $sessionStorage.basketUserId = data.id;
                } else {
                    $sessionStorage.basketUserId = data.id;
                }

                setup();
            })
            .catch(function (data) {
                if (typeof $sessionStorage.basketUserId != 'undefined') {
                    if ($sessionStorage.basketUserId != null) {
                        $sessionStorage.$reset();
                    }
                    $sessionStorage.basketUserId = null;
                } else {
                    $sessionStorage.basketUserId = null;
                }

                setup();
            });

        function setup() {
            if (typeof $sessionStorage.basketCustomer != 'undefined') {
                vm.customer = $sessionStorage.basketCustomer;
            }

            ShopService.getPaymentForms()
                .then(function (data) {
                    vm.paymentFormNames = data;
                    if (typeof $sessionStorage.paymentForm != 'undefined') {
                        vm.paymentForm = $sessionStorage.paymentForm;
                    }

                    vm.paymentFormNames.forEach(function (item) {
                        if (item.id == vm.paymentForm) {
                            vm.deliveryCost = parseFloat(item.cost);
                            vm.deliveryCostString = number_format(vm.deliveryCost, 2, ',', '') + ' zł';
                            vm.deliveryFormString = item.name;
                        }
                    });

                    if (typeof $sessionStorage.basketItems != 'undefined') {
                        vm.items = $sessionStorage.basketItems;
                        vm.items.forEach(function (item) {
                            ShopService.getProduct(item.object.id)
                                .then(function (data) {
                                    if (item.quantity > data.quantity) {
                                        item.quantityError = 'Niewystarczająca ilość artykułu w magazynie. Maksymalna ilość to ' + data.quantity + ' szt.';
                                    } else {
                                        item.quantityError = '';
                                        item.total = item.quantity * data.price;
                                        item.totalString = number_format(item.total, 2, ',', '') + ' zł';
                                    }
                                    item.object = data;

                                    updateTotal()
                                });
                        });
                    }
                });
        }

        function updateTotal() {
            vm.total = 0;

            vm.items.forEach(function (item) {
                item.total = item.quantity * item.object.price;
                item.totalString = number_format(item.total, 2, ',', '') + ' zł';
                vm.total += item.total;
            });

            if (vm.items.length > 0) {
                vm.totalWithDelivery = vm.total + vm.deliveryCost;
            } else {
                vm.totalWithDelivery = 0;
            }

            vm.totalString = number_format(vm.total, 2, ',', '') + ' zł';
            vm.totalWithDeliveryString = number_format(vm.totalWithDelivery, 2, ',', '') + ' zł';
        }

        function updatePaymentForm() {
            $sessionStorage.paymentForm = vm.paymentForm;

            vm.paymentFormNames.forEach(function (item) {
                if (item.id == vm.paymentForm) {
                    vm.deliveryCost = parseFloat(item.cost);
                    vm.deliveryCostString = number_format(vm.deliveryCost, 2, ',', '') + ' zł';
                }
            });

            updateTotal()
        }

        function addToBasket(id) {
            ShopService.getProduct(id)
                .then(function (data) {
                    var index = -1;
                    vm.items.forEach(function (item, i) {
                        if (item.object.id == data.id) {
                            index = i;
                        }
                    });

                    if (index == -1) {
                        var item = {
                            quantity: 1,
                            total: data.price,
                            quantityError: '',
                            object: data
                        };
                        vm.items.push(item)
                    } else {
                        if (vm.items[index].quantity == vm.items[index].object.quantity) {
                            alert('Nie można dodać produktu do koszyka. W magazynie nie ma wiecej egzemplarzy.');
                        } else {
                            vm.items[index].quantity++;
                            vm.items[index].total = vm.items[index].quantity * data.price;
                            vm.items[index].totalString = number_format(vm.items[index].total, 2, ',', '') + ' zł';
                        }
                        vm.items[index].object = data;
                    }

                    $sessionStorage.basketItems = vm.items;

                    updateTotal();
                });
        }

        function removeFromBasket(id) {
            var index = -1;
            vm.items.forEach(function (item, i) {
                if (item.id == id) {
                    index = i;
                }
            });

            if (index > -1) {
                vm.items.splice(index, 1);
            }

            $sessionStorage.basketItems = vm.items;

            updateTotal();
        }

        function checkQuantity($event) {
            var error = false;
            vm.items.forEach(function (item) {
                if (item.quantity > item.object.quantity) {
                    error = true;
                    item.quantityError = 'Niewystarczająca ilość artykułu w magazynie. Maksymalna ilość to ' + item.object.quantity + ' szt.';
                } else {
                    item.quantityError = '';
                }
            });

            if (error) {
                $event.preventDefault();
            }
        }

        function checkUserData($event) {
            vm.userDataError = '';

            if (vm.customer.firstname.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.lastname.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.phone.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.email.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.street.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.zip.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.customer.city.length == 0) {
                vm.userDataError = 'Ilość danych kontaktowych zbyt mała aby kontynuować składanie zamówienia.';
            }

            if (vm.userDataError.length > 0) {
                $event.preventDefault();
            } else {
                $sessionStorage.basketCustomer = vm.customer;
            }
        }

        function storeCustomer() {
            $sessionStorage.basketCustomer = vm.customer;
        }

        function showError(index, error) {
            $('.form-customer-' + index).qtip({
                content: {
                    title: null,
                    text: error
                },
                position: {
                    my: 'bottom left',
                    at: 'top right',
                    adjust: {
                        x: -30
                    }
                },
                show: {
                    ready: true,
                    event: false
                },
                hide: {
                    event: 'click focus unfocus'
                },
                style: {
                    classes: 'qtip-red qtip-rounded qtip-shadow'
                }
            });
        }

        function checkEmail(emailAddress) {
            var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
            return !!re.test(emailAddress);


        }

        function checkPhone(phone) {
            var re = /^[0-9 \-\+() ]+$/i;
            return !!re.test(phone);


        }

        function updateCustomer($event) {
            var error = false;

            $('div[id^="qtip-"]').each(function () {
                var _qtip2 = $(this).data("qtip");
                if (_qtip2 != undefined) {
                    _qtip2.destroy(true);
                }
            });


            if (vm.customer.firstname.length == 0) {
                showError('firstname', 'Wypełnij pole');
                error = true;
            }

            if (vm.customer.lastname.length == 0) {
                showError('lastname', 'Wypełnij pole');
                error = true;
            }

            if (vm.customer.phone.length == 0) {
                showError('phone', 'Wypełnij pole');
                error = true;
            } else {
                if (!checkPhone(vm.customer.phone)) {
                    showError('phone', 'Nieprawidłowy format numeru telefonu');
                    error = true;
                }
            }

            if (vm.customer.email.length == 0) {
                showError('email', 'Wypełnij pole');
                error = true;
            } else {
                if (!checkEmail(vm.customer.email)) {
                    showError('email', 'Nieprawidłowy format adresu e-mail');
                    error = true;
                }
            }

            if (vm.customer.street.length == 0) {
                showError('street', 'Wypełnij pole');
                error = true;
            }

            if (vm.customer.zip.length == 0) {
                showError('zip', 'Wypełnij pole');
                error = true;
            }

            if (vm.customer.city.length == 0) {
                showError('city', 'Wypełnij pole');
                error = true;
            }

            if (error) {
                $event.preventDefault();
            }
        }

        function sendOrder() {
            ShopService.sendOrder(vm.items, vm.paymentForm, vm.customer)
                .then(function (data) {
                    $sessionStorage.$reset();
                    window.location.href = data.url;
                });
        }
    }
})();
