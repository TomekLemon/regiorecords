/**
 * Created by rafalglazar on 21.12.2015.
 */

(function () {
    'use strict';

    angular
        .module('ShopApplication', [
            'ngStorage'
        ]);
})();
