/**
 * Created by rafalglazar on 21.12.2015.
 */
(function () {
    'use strict';

    angular
        .module('ShopApplication')
        .factory('ShopService', ShopService);

    ShopService.$inject = ['$http', '$log', '$q'];

    function ShopService($http, $log, $q) {
        return {
            getPaymentForms: getPaymentForms,
            getProduct: getProduct,
            getUser: getUser,
            sendOrder: sendOrder
        };

        function getPaymentForms() {
            var deferred = $q.defer();

            $http.get(shopApiUrl + 'api/payment/forms')
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    $log.error('Pobranie form płatności  nie powiodło się');

                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function getProduct(productId) {
            var deferred = $q.defer();

            $http.get(shopApiUrl + 'api/product/' + productId)
                .then(function (response) {
                    response.data.price = parseFloat(response.data.price);
                    response.data.quantity = parseInt(response.data.quantity);

                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    $log.error('Pobranie projektu nie powiodło się');

                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function getUser() {
            var deferred = $q.defer();

            $http.get(shopApiUrl + 'api/user/me')
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    $log.error('Pobranie użytkownika nie powiodło się');

                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function sendOrder(items, payment, customer) {
            var deferred = $q.defer();
            var data = {
                items: [],
                payment_form: payment,
                customer: customer
            };

            items.forEach(function (item) {
                var temp = {
                    id: item.object.id,
                    quantity: item.quantity
                };

                data.items.push(temp);
            });

            $http.post(shopApiUrl + 'api/order/send', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    $log.error('Pobranie użytkownika nie powiodło się');

                    deferred.reject(response);
                });

            return deferred.promise;
        }
    }
})();
