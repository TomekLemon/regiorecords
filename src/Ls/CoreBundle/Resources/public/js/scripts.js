$(".news-container").on("click", ".news_big", function () {
    $('.black_mask').show();
    $(this).css('z-index', '9999');
    $(this).find('> div').hide();
    $(this).find('> div.image').show();
    $(this).find('h3').hide();
    $(this).find('.extra_content').show();
    $(this).css('cursor', 'auto');
});

$(".news-container").on("click", ".news_small", function () {
    $('.black_mask').show();
    $(this).css('z-index', '9999');
    $(this).find('> div').hide();
    $(this).find('> div.image').show();
    $(this).find('h3').hide();
    $(this).find('.extra_content').show();
    $(this).css('cursor', 'auto');
});

$(".news-container").on("click", ".close_big", function (e) {
    e.stopPropagation();
    $('.black_mask').hide();
    $(this).parents('.news_big').css('z-index', '5');
    $(this).parents('.news_big').find('div').show();
    $(this).parents('.news_big').find('h3').show();
    $(this).parents('.news_big').find('.extra_content').hide();
    $(this).parents('.news_big').css('cursor', 'pointer');
});

$(".news-container").on("click", ".close_small", function (e) {
    e.stopPropagation();
    $('.black_mask').hide();
    $(this).parents('.news_small').css('z-index', '5');
    $(this).parents('.news_small').find('div').show();
    $(this).parents('.news_small').find('h3').show();
    $(this).parents('.news_small').find('.extra_content').hide();
    $(this).parents('.news_small').css('cursor', 'pointer');
});

$('.black_mask').on('click', function (e) {
    e.stopPropagation();
    $('.close_small').trigger('click');
    $('.close_big').trigger('click');
    $('.send_demo').hide();
});

$('.send_demo_link').on('click', function () {
    if ($('body').width() > 964) {
        $('.black_mask').show();
        $('.send_demo').show();
    }
});

function submitDemoForm() {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var form = $('#form-demo');
    var data = form.serialize();
    var url = form.attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            if (response.result == 'OK') {
                $('.send_demo').find('.content').html(response.form);
                alert('Twoja wiadomość została wysłana.')
            } else {
                showErrors(response.errors, 'demo');
            }
        }
    });

    return false;
}

function sendChangePassword() {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var form = $('#form-change-password');
    var data = form.serialize();
    var url = form.attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            if (response.result == 'OK') {
                $('#changePassword').html(response.form);
                alert('Hasło zostało zmienione.')
            } else {
                showErrors(response.errors, 'change-password');
            }
        }
    });

    return false;
}

function showErrors(errors, formSuffix) {
    var error = null;
    var suberror = null;

    for (var index in errors) {
        if (errors.hasOwnProperty(index)) {
            error = errors[index];

            if (typeof error == 'string') {
                $('.form-' + formSuffix + '-' + index).qtip({
                    content: {
                        title: null,
                        text: error
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top right',
                        adjust: {
                            x: -30
                        }
                    },
                    show: {
                        ready: true,
                        event: false
                    },
                    hide: {
                        event: 'click focus unfocus'
                    },
                    style: {
                        classes: 'qtip-red qtip-rounded qtip-shadow'
                    }
                });
            } else {
                for (var subindex in error) {
                    if (error.hasOwnProperty(subindex)) {
                        suberror = error[subindex];

                        if (typeof suberror == 'string') {
                            $('.form-' + formSuffix + '-' + index + '-' + subindex).qtip({
                                content: {
                                    title: null,
                                    text: suberror
                                },
                                position: {
                                    my: 'bottom left',
                                    at: 'top right',
                                    adjust: {
                                        x: -30
                                    }
                                },
                                show: {
                                    ready: true,
                                    event: false
                                },
                                hide: {
                                    event: 'click focus unfocus'
                                },
                                style: {
                                    classes: 'qtip-red qtip-rounded qtip-shadow'
                                }
                            });
                        }
                    }
                }
            }
        }
    }
}

function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}