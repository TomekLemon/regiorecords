$(function () {

    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: '/bundles/lscore/css/editor.css',
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}}
        ],
        filebrowserBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash'
    };

    fckconfig_news = {
        skin: 'BootstrapCK-Skin',
        contentsCss: '/bundles/lscore/css/editor-news.css',
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}}
        ],
        filebrowserBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lscms/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        width: 'auto',
        height: '400px'
    }, fckconfig_common);

    fckconfig_news = jQuery.extend(true, {
        width: 'auto',
        height: '400px'
    }, fckconfig_news);

    $('.wysiwyg').ckeditor(fckconfig);
    $('.wysiwyg-news').ckeditor(fckconfig_news);
});