<?php

namespace Ls\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class BackendMenu extends ContainerAware {

    protected $containerBuilder;

    public function managedMenu(FactoryInterface $factory) {
        $request = $this->container->get('request');
        $blocks = $this->container->getParameter('ls_core.admin.menu');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $route = $request->get('_route');
        $set_current = false;
        foreach ($blocks as $block) {
            $parent = $menu->addChild($block['title'], array(
                'uri' => '#',
                'linkAttributes' => array(
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown'
                )
            ));
            $parent->setAttribute('class', 'dropdown');
            foreach ($block['links'] as $link) {
                $service = $this->container->get($link);
                $set_current = $service->addToMenu($menu, $parent, $route, $set_current);
            }
        }

        if (!$set_current) {
            $menu->setCurrentUri($this->container->get('request')->getRequestUri());
        }

        return $menu;
    }
}

