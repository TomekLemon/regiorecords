<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Entity\TemporaryFile;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = 8;
        $allow = 1;

        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
            ->from('LsNewsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($news) < $limit) {
            $allow = 0;
        }

        return $this->render('LsCoreBundle:Default:index.html.twig', array(
            'allow' => $allow,
            'news' => $news,
        ));
    }

    public function newsMoreAction() {
        $em = $this->getDoctrine()->getManager();

        $page = $this->get('request')->request->get('page');
        $limit = 6;
        $start = ($page * $limit) + 2;
        $allow = 1;

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('n')
            ->from('LsNewsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsCoreBundle:Default:news.html.twig', array(
                'news' => $entities,
                'page' => $page,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

    public function contactAction() {
        $em = $this->getDoctrine()->getManager();

        return $this->render('LsCoreBundle:Default:contact.html.twig', array());
    }

    public function temporaryFileUploadAction() {
        $em = $this->getDoctrine()->getManager();
        $filename = $this->get('request')->request->get('filename');

        $temporaryFile = new TemporaryFile();
        $temporaryFile->setFilename($filename);

        $em->persist($temporaryFile);
        $em->flush();

        return $this->render('LsCoreBundle:Default:file.html.twig', array(
            'entity' => $temporaryFile,
        ));
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }
}
