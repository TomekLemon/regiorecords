<?php

namespace Ls\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemoAttachment
 * @ORM\Table(name="demo_attachment")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class DemoAttachment {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Demo",
     *     inversedBy="attachments"
     * )
     * @ORM\JoinColumn(
     *     name="demo_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\DemoBundle\Entity\Demo
     */
    private $demo;

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DemoAttachment
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set demo
     *
     * @param \Ls\DemoBundle\Entity\Demo $demo
     * @return DemoAttachment
     */
    public function setDemo(Demo $demo = null) {
        $this->demo = $demo;

        return $this;
    }

    /**
     * Get demo
     *
     * @return \Ls\DemoBundle\Entity\Demo
     */
    public function getDemo() {
        return $this->demo;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }

    public function getAttachmentWebPath() {
        if (null === $this->name || empty($this->name)) {
            return false;
        } else {
            return '/' . $this->getUploadDir() . '/' . $this->name;
        }
    }

    public function getAttachmentAbsolutePath() {
        if (null === $this->name || empty($this->name)) {
            return false;
        } else {
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->name;
        }
    }

    public function getUploadRootDir() {
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'upload/demo/' . $this->getDemo()->getId();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeAttachment() {
        $file = $this->getAttachmentAbsolutePath();
        if ($file) {
            @unlink($file);
        }
    }
}
