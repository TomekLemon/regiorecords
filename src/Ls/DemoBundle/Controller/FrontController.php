<?php

namespace Ls\DemoBundle\Controller;

use Ls\CoreBundle\Utils\Tools;
use Ls\DemoBundle\Entity\Demo;
use Ls\DemoBundle\Entity\DemoAttachment;
use Ls\DemoBundle\Form\DemoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function sendAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        $response = array(
            'result' => 'ERROR',
            'form' => '',
            'errors' => array()
        );

        $form = $this->createForm(new DemoType(), null, array(
            'action' => $this->container->get('router')->generate('ls_demo_send'),
            'method' => 'PUT',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $name = $form->get('name')->getData();
            $email = $form->get('email')->getData();
            $content = $form->get('content')->getData();
            $files = $request->get('files');

            $entity = new Demo();
            $entity->setName($name);
            $entity->setEmail($email);
            $entity->setContent($content);
            $em->persist($entity);
            $em->flush();

            if (is_array($files)) {
                foreach ($files as $file_id) {
                    $oTempFile = $em->getRepository('LsCoreBundle:TemporaryFile')->findOneById($file_id);

                    $oAttachment = new DemoAttachment();
                    $oAttachment->setDemo($entity);
                    $oAttachment->setName($oTempFile->getFilename());
                    $em->persist($oAttachment);
                    $em->flush();

                    if (!is_dir($oAttachment->getUploadRootDir())) {
                        $old_umask = umask(0);
                        mkdir($oAttachment->getUploadRootDir(), 0777);
                        umask($old_umask);
                    }

                    @rename($oTempFile->getFileAbsolutePath(), $oAttachment->getAttachmentAbsolutePath());

                    $oTempFile->deleteFile();
                    $em->remove($oTempFile);
                    $em->flush();
                }
            }

            $url = $this->get('router')->generate('ls_core_admin', array(), true);

            $message_txt = '<h3>Dodano nowe demo:</h3>';
            $message_txt .= '<b>Imię/Nazwa zespołu:</b> ' . $form->get('name')->getData() . '<br />';
            $message_txt .= '<b>Adres e-mail:</b> ' . $form->get('email')->getData() . '<br /><br />';
            $message_txt .= 'Szczegóły i załączniki można obejrzeć w <a href="' . $url . '">Panelu administracyjnym</a>';

            $email_to = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_demo')->getValue();

            $message = \Swift_Message::newInstance();
            $message->setSubject('Nowe demo');
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email_to);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $form = $this->createForm(new DemoType(), null, array(
                'action' => $this->container->get('router')->generate('ls_demo_send'),
                'method' => 'PUT',
            ));

            $response['form'] = iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsDemoBundle::demo_form.html.twig', array(
                'form' => $form->createView()
            ))->getContent());

            $response['result'] = 'OK';
        } else {
            $response['errors'] = Tools::getFormErrors($form);
        }

        return new JsonResponse($response);
    }
}
