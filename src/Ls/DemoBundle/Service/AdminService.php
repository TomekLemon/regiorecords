<?php

namespace Ls\DemoBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $menu, ItemInterface $parent, $route, $set) {
        $router = $this->container->get('router');

        $parent->addChild('Wysłane dema', array(
            'route' => 'ls_admin_demo',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_demo':
                case 'ls_admin_demo_show':
                    $menu->setCurrentUri($router->generate('ls_admin_demo'));
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Wysłane dema');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_demo')));
    }
}

