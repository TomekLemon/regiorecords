<?php

namespace Ls\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class DemoType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Imię/Nazwa zespołu:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('email', null, array(
                'label' => 'Twój adres e-mail:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                    new Email(array(
                        'message' => 'Podaj poprawny adres e-mail'
                    ))
                )
            )
        );
        $builder->add('content', 'textarea', array(
                'label' => 'Treść wiadomości:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('submit', 'submit', array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\DemoBundle\Entity\Demo',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_front_demo';
    }
}
