<?php

namespace Ls\CatalogBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Catalog
 * @ORM\Table(name="catalog")
 * @ORM\Entity
 */
class Catalog {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"author", "title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $premier;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    protected $file;

    protected $listWidth           = 100;
    protected $listHeight          = 100;
    protected $detailDesktopWidth  = 397;
    protected $detailDesktopHeight = 397;
    protected $detailMobileWidth   = 224;
    protected $detailMobileHeight  = 224;

    public function __construct() {
        $this->created_at = new \DateTime();
        $this->premier = new \DateTime();
        $this->seo_generate = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Catalog
     */
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Catalog
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Catalog
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set premier
     *
     * @param \DateTime $premier
     * @return Catalog
     */
    public function setPremier($premier) {
        $this->premier = $premier;
        return $this;
    }

    /**
     * Get premier
     *
     * @return \DateTime
     */
    public function getPremier() {
        return $this->premier;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Catalog
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Catalog
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Catalog
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Catalog
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Catalog
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Catalog
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Catalog
     */
    public function setCreatedAt($createdAt = null) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Catalog
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail_desktop':
                $size['width'] = $this->detailDesktopWidth;
                $size['height'] = $this->detailDesktopHeight;
                break;
            case 'detail_mobile':
                $size['width'] = $this->detailMobileWidth;
                $size['height'] = $this->detailMobileHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName .= Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_dd');
                    break;
                case 'detail_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_dm');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName .= Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail_desktop':
                    $sThumbName .= Tools::thumbName($this->photo, '_dd');
                    break;
                case 'detail_mobile':
                    $sThumbName .= Tools::thumbName($this->photo, '_dm');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_dd = Tools::thumbName($filename, '_dd');
            $filename_dm = Tools::thumbName($filename, '_dm');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_dd)) {
                @unlink($filename_dd);
            }
            if (file_exists($filename_dm)) {
                @unlink($filename_dm);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/catalog';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs() {
        $sFileName = $this->getPhoto();
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'] + 2, $aThumbSizeL['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameDD = Tools::thumbName($sSourceName, '_dd');
        $aThumbSizeDD = $this->getThumbSize('detail_desktop');
        $thumb->adaptiveResize($aThumbSizeDD['width'] + 2, $aThumbSizeDD['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeDD['width'], $aThumbSizeDD['height']);
        $thumb->save($sThumbNameDD);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameDM = Tools::thumbName($sSourceName, '_dm');
        $aThumbSizeDM = $this->getThumbSize('detail_mobile');
        $thumb->adaptiveResize($aThumbSizeDM['width'] + 2, $aThumbSizeDM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeDM['width'], $aThumbSizeDM['height']);
        $thumb->save($sThumbNameDM);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
}