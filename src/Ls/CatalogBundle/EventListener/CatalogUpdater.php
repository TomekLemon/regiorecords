<?php

namespace Ls\CatalogBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CatalogBundle\Entity\Catalog;
use Ls\CoreBundle\Utils\Tools;

class CatalogUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Catalog) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getAuthor() . ' ' . $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getAuthor() . ' - ' . $entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Catalog) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getAuthor() . ' ' . $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getAuthor() . ' - ' . $entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Catalog) {
            $entity->deletePhoto();
        }
    }

}