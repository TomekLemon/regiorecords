<?php

namespace Ls\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Catalog controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Catalog entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('k')
            ->from('LsCatalogBundle:Catalog', 'k')
            ->orderBy('k.premier', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCatalogBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Catalog entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsCatalogBundle:Catalog', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Catalog entity.');
        }

        return $this->render('LsCatalogBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
}
