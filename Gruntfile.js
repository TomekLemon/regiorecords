module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                sourceMap: true,
                beautify: true
            },
            main: {
                src: [
                    'src/Ls/CoreBundle/Resources/public/js/shopApp/*.js'
                ],
                dest: 'src/Ls/CoreBundle/Resources/public/js/shopApp.min.js'
            }
        },
        watch: {
            scripts: {
                files: [
                    'src/Ls/CoreBundle/Resources/public/js/shopApp/*.js'
                ],
                tasks: ['uglify:main']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['uglify']);
};
